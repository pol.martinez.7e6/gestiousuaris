<?php
session_start();
include("../database/database.php");

// Obtener datos de usuarios
$sql = "SELECT * FROM usuari";
$result = $conn->query($sql);

if (isset($_SESSION['usu_nom'])) {
    if ($_SESSION['usu_nivell'] == "admin") {
        if ($result->num_rows > 0) {
            echo "<table style='border-collapse: collapse;'>";
            echo "<tr><th style='border: 1px solid black; padding: 10px;'>Nom d'usuari</th>";
            echo "<th style='border: 1px solid black; padding: 10px;'>Nivell d'usuari</th>";
            echo "<th style='border: 1px solid black; padding: 10px;'>Modificar</th>";
            echo "<th style='border: 1px solid black; padding: 10px;'>Borrar</th></tr>";
            while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                <td style='border: solid black 1px; padding: 5px;'><?php echo $row['usu_nom'];?> </td>
                <td style='border: solid black 1px; padding: 5px;'><?php echo $row["usu_nivell"]; ?></td>
                <td style='border: solid black 1px; padding: 5px;'>
                <a href='modificarUsuaris.php?id=<?php echo $row['id'] ?>'>Modificar</a>
                </td>
                <td style='border: solid black 1px; padding: 5px;'>
                <a href='eliminaUsuaris.proc.php?id=<?php echo $row['id'] ?>' onclick="return confirm('Segur que vols borrar?')">Borrar</a>
                </td>
                </tr>
                <?php
            }

            echo "<tr style='border: solid black 1px; padding: 5px;'><td><a href='afegeixUsuaris.php'>Afegeix Usuari</a></td></tr>";
            echo "</table>";
            
        } else {
            echo "No hi ha usuaris";
        }

        echo "<a href='logout.proc.php' style = 'margin-top: 2%;'>Logout</a>";

        $conn->close();
    } else {
        header('location: error.php?msg=No tens permisos de admin!');
    }
} else {
    header("location: login.html");
}
?>